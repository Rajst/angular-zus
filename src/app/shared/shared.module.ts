import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YesnoPipe } from './pipes/yesno.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardComponent } from './components/card/card.component';
import { TabsModule } from './modules/tabs/tabs.module';
import { DropdownModule } from './dropdown/dropdown.module';
import { RecentSearchComponent } from './containers/recent-search/recent-search.component';



@NgModule({
  declarations: [
    YesnoPipe,
    CardComponent,
    RecentSearchComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule,
    DropdownModule,
  ],
  exports: [
    YesnoPipe,
    FormsModule,
    ReactiveFormsModule,
    CardComponent,
    TabsModule,
    DropdownModule,
    RecentSearchComponent,
  ]
})
export class SharedModule { }
