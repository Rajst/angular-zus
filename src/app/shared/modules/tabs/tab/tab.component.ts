import { Component, Input, OnInit } from '@angular/core';
import { TabsComponent } from '../tabs/tabs.component';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  @Input() title: string = ''

  isOpen = false

  toggle() {
    // this.isOpen = !this.isOpen
    this.parent.toggle(this)
  }

  constructor(private parent: TabsComponent) {
    // console.log(parent)
    parent.registerTab(this)
  }

  ngOnInit(): void {
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.

    this.parent.unregisterTab(this)
  }

}
