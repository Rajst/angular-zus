import { Component, OnInit } from '@angular/core';
import { MusicApiService } from 'src/app/core/services/music-api/music-api.service';

@Component({
  selector: 'app-recent-search',
  templateUrl: './recent-search.component.html',
  styleUrls: ['./recent-search.component.scss']
})
export class RecentSearchComponent implements OnInit {

  queries = [
    'alice','bob'
  ]

  constructor(
    private service: MusicApiService
  ) { 
    this.service.queryChange.subscribe(query=>{
      this.queries.push(query)
      this.queries = this.queries.slice(-5)
    })
  }

  search(query: string) {
    this.service.searchAlbums(query)
  }

  ngOnInit(): void {
  }

}
