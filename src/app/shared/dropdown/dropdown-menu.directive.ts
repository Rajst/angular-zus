import { Directive, ElementRef, HostBinding, HostListener, Input, Optional } from '@angular/core';
import { DropdownDirective } from './dropdown.directive';

@Directive({
  selector: '[appDropdownMenu]',
  exportAs: 'appDropdownMenu'
})
export class DropdownMenuDirective {
  @HostBinding('class.show')
  isOpen = false;

  @Input() autoClose = true

  constructor(
    @Optional() private parent: DropdownDirective | null,
    private elem: ElementRef<HTMLElement>) {
    // console.log('appDropdownMenu', elem)
    if (parent !== null) {
      this.parent!.menu = this
    }
  }

  toggle() {
    // this.elem.nativeElement.classList.toggle('show')
    this.isOpen = !this.isOpen
    this.toggled = true
  }
  toggled = false 


  @HostListener('document:click', ['$event.path'])
  documentClick(path: HTMLElement[]) {
    if (this.autoClose == false) { return }
    if (this.toggled) { this.toggled = false; return }

    if (this.isOpen) {//} && !path.includes(this.elem.nativeElement)) {
      this.isOpen = false
    }
  }

}
