import { Directive } from '@angular/core';
import { DropdownMenuDirective } from './dropdown-menu.directive';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  menu?: DropdownMenuDirective;

  constructor() {
    console.log('hello appDropdown')
  }

}
