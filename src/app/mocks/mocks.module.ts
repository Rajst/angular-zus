import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { mockAlbums } from './mockAlbums';
import { INITIAL_ALBUMS } from '../core/tokens';
import { MocksRoutingModule } from './playlists-routing.module';
import { SandboxComponent } from './components/sandbox/sandbox.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    SandboxComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MocksRoutingModule
  ],
  providers: [
    {
      provide: INITIAL_ALBUMS,
      useValue: mockAlbums
    }
  ]
})
export class MocksModule { }
