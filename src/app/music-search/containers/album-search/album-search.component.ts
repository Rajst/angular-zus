import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Album } from 'src/app/core/model/Album';
import { MusicApiService } from 'src/app/core/services/music-api/music-api.service';


@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.scss']
})
export class AlbumSearchComponent implements OnInit {
  message = ''
  loading = false
  open = false

  query = this.service.queryChange
  results: Observable<Album[]> = this.service.resultsChange

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicApiService) { }

  searchAlbums(query: string) {
    // this.service.searchAlbums(query)

    this.router.navigate([/* '/search' */], {
      queryParams: { q: query },
      relativeTo: this.route,
      replaceUrl: true
    })
  }

  ngOnInit(): void {
    // const q = this.route.snapshot.queryParamMap.get('q')
    this.route.queryParamMap.subscribe(paramMap => {
      const q = paramMap.get('q')
      if (q) {
        this.service.searchAlbums(q)
      }
    })
  }
}


// this.results = this.service.fetchSearchAlbums(query)
// .pipe(shareReplay()) 