import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { AlbumSearchComponent } from './containers/album-search/album-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { INITIAL_ALBUMS } from '../core/tokens';
import { SharedModule } from '../shared/shared.module';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';

@NgModule({
  declarations: [
    AlbumSearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent,
    AlbumDetailsComponent
  ],
  imports: [
    CommonModule,
    MusicSearchRoutingModule,
    SharedModule
  ],
  providers: [
    {
      provide: INITIAL_ALBUMS,
      useValue: []
    }
  ]
})
export class MusicSearchModule { }
