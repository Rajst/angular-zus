import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  FormArray, FormControl, FormControlDirective, FormGroup, Validators, ValidatorFn,
  AbstractControl, ValidationErrors, AsyncValidatorFn
} from '@angular/forms';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, withLatestFrom } from 'rxjs/operators';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  advancedOpen = false

  @Input() set query(query: string | null) {
    if (query == null) { return }
    const field = (this.searchForm.get('query') as FormControl)
    const validator = field.asyncValidator
    field.setAsyncValidators([])
    field.setValue(query, {
      emitEvent: false,
      // onlySelf: true
    })
    field.setAsyncValidators(validator)
  }

  @Output() search = new EventEmitter<string>();

  censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    if ('string' !== typeof control.value) { throw 'Only strings!' }
    const badword = 'batman'
    const hasError = control.value.includes(badword)

    return hasError ? { censor: { badword } } : null
  }

  censorAsync: AsyncValidatorFn = (control) => {
    if ('string' !== typeof control.value) { throw 'Only strings!' }
    const badword = 'batman'

    // return this.http.get('').pipe(map(resp => resp.error ))
    return new Observable((observer) => {
      // console.log('Subscribed ' + control.value)
      const value = control.value;

      const handle = setTimeout(() => {
        const hasError = value.includes(badword)

        // console.log('Next ' + value)
        observer.next(hasError ? { censor: { badword } } : null)
        observer.complete()
      }, 1000)

      /* TearDown Logic */
      return () => {
        //console.log('Unsubscribed');
        clearTimeout(handle)
      }
    })
    //.subscribe({next,error,complete}).unsubscribe()
  }

  searchForm = new FormGroup({
    'query': new FormControl('', [
      // Validators.required,
      Validators.minLength(3),
      // this.censor
      // Validators.pattern('[a-zA-Z ]*')
    ], [
      this.censorAsync
    ]),
    'options': new FormGroup({
      'type': new FormControl('album'),
      'markets': new FormArray([
        new FormGroup({
          'code': new FormControl('PL'),
        }),
      ])
    })
  })
  markets = this.searchForm.get(['options', 'markets']) as FormArray

  constructor() {
    (window as any).form = this.searchForm;

    const field = (this.searchForm.get('query') as FormControl);

    const validChange = field.statusChanges.pipe(
      filter(status => status === 'VALID')
    ) as Observable<'VALID'>

    const valueChange = field.valueChanges.pipe(
      debounceTime(400),
      distinctUntilChanged(),
    ) as Observable<string>

    const searchChange = validChange.pipe(
      withLatestFrom(valueChange),
      map(([valid, value]) => value)
    );

    searchChange
      .subscribe(this.search)
    // .subscribe(console.log)

  }

  addMarket() {
    this.markets.push(new FormGroup({
      'code': new FormControl(''),
    }))
  }
  removeMarket(index: number) {
    this.markets.removeAt(index)
  }

  submitQuery(query: string) {
    this.search.emit(query)
  }

  ngOnInit(): void {
  }

}
