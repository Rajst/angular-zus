import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PlaylistDetailsComponent } from '../../components/playlist-details/playlist-details.component';
import { PlaylistFormComponent } from '../../components/playlist-form/playlist-form.component';
import { PlaylistListComponent } from '../../components/playlist-list/playlist-list.component';

import { PlaylistsViewComponent } from './playlists-view.component';

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { By } from '@angular/platform-browser';

@Component({ selector: 'app-playlist-list', template: `<div>mock playlist</div>`, })
export class PlaylistListComponentMock {
  @Input() selectedId: any;
  @Input() items = [];
  @Output() selectedIdChange = new EventEmitter<string>();
  constructor() { }
}

@Component({ selector: 'app-playlist-details', template: `<div>mock details</div>`, })
export class PlaylistDetailsComponentMock {
  @Input() playlist: any;
  @Output() edit = new EventEmitter<string>();
  constructor() { }
}


fdescribe('PlaylistsViewComponent', () => {
  let component: PlaylistsViewComponent;
  let fixture: ComponentFixture<PlaylistsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        PlaylistsViewComponent,
        // PlaylistListComponent,
        PlaylistListComponentMock,
        PlaylistDetailsComponentMock,
        PlaylistFormComponent
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select playlist', () => {
    const mock = fixture.debugElement.query(By.directive(PlaylistListComponentMock))

    expect(mock.componentInstance.selectedId).toEqual(null)
    mock.componentInstance.selectedIdChange.emit('123')
    fixture.detectChanges()

    const mockdetails = fixture.debugElement.query(By.directive(PlaylistDetailsComponentMock))
    expect(mockdetails).toBeDefined()
    expect(mockdetails.componentInstance.playlist.id).toEqual('123')
    expect(mock.componentInstance.selectedId).toEqual('123')

    expect(component).toBeTruthy();
  });
});
