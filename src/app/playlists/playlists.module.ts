import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistListComponent } from './components/playlist-list/playlist-list.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistFormComponent } from './components/playlist-form/playlist-form.component';
import { PlaylistsViewComponent } from './containers/playlists-view/playlists-view.component';
import { PlaylistListItemComponent } from './components/playlist-list-item/playlist-list-item.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    PlaylistListComponent,
    PlaylistDetailsComponent,
    PlaylistFormComponent,
    PlaylistsViewComponent,
    PlaylistListItemComponent
  ],
  imports: [
    CommonModule,
    PlaylistsRoutingModule,
    SharedModule
  ],
  // exports:[
  //   PlaylistsViewComponent
  // ]
})
export class PlaylistsModule { }
