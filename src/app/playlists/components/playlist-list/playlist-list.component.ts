import { NgForOf, NgForOfContext } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Playlist } from 'src/app/core/model/Playlist';

NgForOf
NgForOfContext

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  // inputs: ['playlists:items']
})
export class PlaylistListComponent implements OnInit {

  @Input('items') playlists: Playlist[] | null= []

  @Input() selectedId?: Playlist['id']

  @Output() selectedIdChange = new EventEmitter<Playlist['id']>()

  @Output() remove = new EventEmitter<Playlist['id']>();

  select(selectedId: string) {
    this.selectedIdChange.emit(selectedId)
  }

  removeClick(playlist_id: Playlist['id'], event: MouseEvent) {
    event.stopPropagation()
    this.remove.emit(playlist_id)
  }

  constructor() {
    // setInterval(() => {
    //   this.playlists.push(this.playlists.shift()!)
    // }, 1000)
  }

  ngOnInit(): void {
  }

}
