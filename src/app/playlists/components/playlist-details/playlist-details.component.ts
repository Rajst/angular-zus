import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';


@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input() playlist!: Playlist

  @Output() edit = new EventEmitter();

  clickEdit(){
    this.edit.emit(this.playlist)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
