import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { CardComponent } from 'src/app/shared/components/card/card.component';

import { PlaylistFormComponent } from './playlist-form.component';

fdescribe('PlaylistFormComponent', () => {
  let component: PlaylistFormComponent;
  let fixture: ComponentFixture<PlaylistFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PlaylistFormComponent, CardComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistFormComponent);
    component = fixture.componentInstance;
    component.playlist = {
      id: '123', name: 'Placki', description: 'desc', public: true
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render form with playlist details', async () => {
    await fixture.whenStable() // wait for ngModel to apply changes (async!)

    const elem = fixture.debugElement.query(By.css('[name="name"]')).nativeElement
    expect(elem.value).toEqual('Placki');

    const elem1 = fixture.debugElement.query(By.css('[name="public"]')).nativeElement
    expect(elem1.checked).toEqual(true);

    const elem2 = fixture.debugElement.query(By.css('[name="description"]')).nativeElement
    expect(elem2.value).toEqual('desc');
  });

  it('should validate name', async () => {
    await fixture.whenStable() // wait for ngModel to apply changes (async!)
    const elem = fixture.debugElement.query(By.css('[name="name"]'))//.nativeElement
    elem.nativeElement.value = ''
    elem.triggerEventHandler('input', {
      target: elem.nativeElement
    })
    fixture.detectChanges()
    // https://jsfiddle.net/bblackwo/4o5u5Lmo/16/
    expect(elem.classes).toEqual(jasmine.objectContaining({ 'ng-invalid': true }))

    const elemErr = fixture.debugElement.query(By.css('.text-danger'))?.nativeElement
    expect(elemErr).toBeDefined()
    expect(elemErr.innerText).toMatch('Name is required')

    elem.nativeElement.value = 'POprawne'
    elem.triggerEventHandler('input', {
      target: elem.nativeElement
    })
    fixture.detectChanges()
    const elemErr2 = fixture.debugElement.query(By.css('.text-danger'))?.nativeElement
    expect(elemErr2).not.toBeDefined()
    expect(elem.classes).not.toEqual(jasmine.objectContaining({ 'ng-invalid': true }))

  })
});
