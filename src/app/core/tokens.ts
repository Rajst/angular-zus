import { InjectionToken } from '@angular/core';
import { Album } from './model/Album';


export const API_URL = new InjectionToken<string>('SEARCH_API_URL');
export const INITIAL_ALBUMS = new InjectionToken<Album[]>('INITIAL_ALBUMS');
