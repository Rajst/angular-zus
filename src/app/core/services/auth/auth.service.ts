import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn = new BehaviorSubject(false)

  constructor(private oauth2: OAuthService) {
    this.oauth2.configure(environment.authConfig)
    // this.oauth2.setupAutomaticSilentRefresh();
  }

  async init() {
    this.oauth2.events.subscribe(event => {
      switch (event.type) {
        case 'logout': return this.isLoggedIn.next(false)
      }
    })
    await this.oauth2.tryLogin({})
    const token = this.oauth2.getAccessToken()
    this.isLoggedIn.next(!!token)
  }

  getToken() {
    return this.oauth2.getAccessToken()
  }

  async login() {
    // this.oauth2.initLoginFlow()
    // this.oauth2.initLoginFlowInPopup()
    await this.oauth2.initImplicitFlowInPopup()
    this.isLoggedIn.next(true)
  }

  logout() {
    this.oauth2
    this.oauth2.logOut()
    this.isLoggedIn.next(false)
  }


}
