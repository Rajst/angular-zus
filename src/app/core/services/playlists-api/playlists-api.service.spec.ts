import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { Playlist } from '../../model/Playlist';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PlaylistsApiService } from './playlists-api.service';
import { API_URL } from '../../tokens';
import { UsersApiService } from '../users-api/users-api.service';

fdescribe('PlaylistsApiService', () => {
  let service: PlaylistsApiService;
  let controller: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        { provide: API_URL, useValue: 'MOCKAPI' },
        { provide: UsersApiService, useValue:{
          getCurrentUser: jasmine.createSpy().and.returnValue(
            of({id:'FAKE_USER_ID'})
          )
        }}
      ]
    });
    service = TestBed.inject(PlaylistsApiService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should fetch playlists', () => {
    const spy = jasmine.createSpy()

    const obs: Observable<Playlist[]> = service.fetchPlaylists()
    obs.subscribe(spy)

    const req = controller.expectOne('MOCKAPI/me/playlists')

    const fakePlaylists = [{ name: 'Fake Playlist' }];
    req.flush({ items: fakePlaylists })

    expect(spy).toHaveBeenCalledWith(fakePlaylists)
  });

  it('should fetch playlist by id', () => {
    const spy = jasmine.createSpy()

    const obs: Observable<Playlist> = service.fetchPlaylistById('123')
    obs.subscribe(spy)

    const req = controller.expectOne('MOCKAPI/playlists/123')
    // expect(req.request.params.get(''))

    const fakePlaylist = { name: 'Fake Playlist' };
    req.flush(fakePlaylist)

    expect(spy).toHaveBeenCalledWith(fakePlaylist)
  });

  it('should save updated playlist', () => {
    const spy = jasmine.createSpy()

    const draft: Playlist = {
      id: '123', name: 'Test', public: false,
      description: 'desc'
    }
    const obs: Observable<Playlist> = service.savePlaylist(draft)
    obs.subscribe(spy)

    const req = controller.expectOne({
      method: 'PUT', url: 'MOCKAPI/playlists/123'
    })
    expect(req.request.body).toEqual({
      name: 'Test',
      public: false,
      description: 'desc'
    })

    const fakePlaylist = { ...draft };
    req.flush(fakePlaylist)

    expect(spy).toHaveBeenCalledWith(fakePlaylist)
  });

  it('should save new playlist', () => {
    const spy = jasmine.createSpy()

    const draft: Playlist = {
      id: '', name: 'Test', public: false,
      description: 'desc'
    }
    const obs: Observable<Playlist> = service.savePlaylist(draft)
    obs.subscribe(spy)

    const req = controller.expectOne({
      method: 'POST', url: 'MOCKAPI/users/FAKE_USER_ID/playlists'
    })
    expect(req.request.body).toEqual({
      name: 'Test',
      public: false,
      description: 'desc'
    })
    const fakePlaylist = {
      id: 'NEW_FAKE_ID',
      name: 'Test',
      public: false,
      description: 'desc'
    };

    req.flush(fakePlaylist)

    expect(spy).toHaveBeenCalledWith(fakePlaylist)
  });
});
