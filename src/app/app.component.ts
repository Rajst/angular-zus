import { ChangeDetectorRef, Component } from '@angular/core';
import { AuthService } from './core/services/auth/auth.service';
import { UsersApiService } from './core/services/users-api/users-api.service';

@Component({
  selector: 'app-root, .placki[sos=malinowy]',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-zus';

  userChange = this.userService.getCurrentUser()
  isOpen = false;

  constructor(private auth: AuthService, private userService: UsersApiService) {

    // setInterval(() => {
    //   this.timer = (new Date()).toLocaleTimeString()
    // }, 2000)
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
  }

  login() {
    this.auth.login()
  }
  
  logout() {
    this.auth.logout()
  }
}
