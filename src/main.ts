import { enableProdMode, LOCALE_ID } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import { registerLocaleData } from '@angular/common';

import localeData from '@angular/common/locales/pl'
registerLocaleData(localeData)

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule, {
  providers: [
    // { provide: LOCALE_ID, useValue: 'en-US' },
    // { provide: LOCALE_ID, useValue: 'pl' },
  ]
})
  .catch(err => console.error(err));