
## ANkieta
Prosze wypełnic:
https://forms.gle/i8hwAysDzfRJ77e67

## Git
cd ..
git clone https://bitbucket.org/ev45ive/angular-zus.git angular-zus
cd angular-zus
npm i 
npm start

https://bitbucket.org/ev45ive/angular-zus/

## GIT pull
git stash -u 
git pull -f

## GIT log
git log  
git log --oneline
git log --oneline -7 --all --graph --decorate
$ git log --oneline -7 --all --decorate --graph

```git
* 837b33a (HEAD -> master) Polecenia GIT
* 853443c (origin/master) Slajdy
* bfe12d2 Directives for reusable logic - dropdown, collapse, etc..
* 21d246e Dropdown with parent + document click host listener
| * 99a9613 (nowa_galaz) zmiany
|/
* 5a9137f Direcrives, HostBinding, HostListener, ElementRef
* 54af487 Cleanup
```

q - exit
git show 5a9137f

Cofamy sie do starej wersji
git checkout 5a9137f

Robimy odgałęzienie od starej wersji
git switch -c <new-branch-name>
... robimy zmiany ...
git add .
git commit -m "opis zmian"

Wracamy do aktualnej wersji 
git checkout master -f 

## Instalacje 
View -> Terminal (Ctrl + `)

node -v 
v14.16.1

npm -v 
6.14.6

git --version 
git version 2.28.0.windows.1

Help -> About
code -v 
1.55.0
c185983a683d14c396952dd432459097bc7f757f
x64

## Visual Studio Code
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode
https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher

QuickType:
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console

## Chrome
https://augury.rangle.io/

## Angular CLI
https://cli.angular.io/ 

npm install -g @angular/cli

echo $PATH
echo %PATH%
... C:\Users\PC\AppData\Roaming\npm\ ...

ng --version
Angular CLI: 11.2.8

## Powershell error
File C:\Users\PC\AppData\Roaming\npm\ng.ps1 cannot be loaded.
ng.cmd --version

ng help

## Update
https://update.angular.io/?l=2&v=10.0-11.0

## Ng new
ng new --directory . --strict --routing --style scss angular-zus

? What name would you like to use for the new workspace and initial project? angular-zus
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS   [ https://sass-lang.com/documentation/syntax#scss  
...
✔ Packages installed successfully.

## GIT 
git config --global user.name "Imie nazwisko"
git config --global user.email "jakis@email"

## Start
ng s --help
ng s -o
npm run start -- -o 
npm run start -- -o --port 4200

✔ Browser application bundle generation complete.
√ Compiled successfully.

** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **
Ctrl+C - stop server


## Browser compability
https://kangax.github.io/compat-table/es6/
https://caniuse.com/
https://angular.io/guide/deployment#differential-loading
https://angular.io/guide/deployment#configuring-differential-loading

not IE 11 # Angular supports IE 11 only as an opt-in. To opt-in, remove the 'not' prefix on this line.


## Change detection
// ExpressionChangedAfterItHasBeenCheckedError NG0100
// https://angular.io/errors/NG0100
https://github.com/angular/angular/tree/master/packages/zone.js

## Generators
ng g
ng g c --help
ng g m --help

ng g m playlists -m app --routing

ng g c playlists/components/playlist-list
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form
ng g c playlists/components/playlist-list-item

ng g c playlists/containers/playlists-view


## bootstrap css
https://sass-lang.com/

npm install bootstrap

// styles.scss:
@import '~bootstrap/dist/css/bootstrap.css';

// package.json:
"architect": {
        "build": {
….
 "styles": [
              "/node_modules/bootstrap/dist/css/bootstrap.css",
              "src/styles.scss"
            ],


## Pipes  / Shared module

ng g m shared -m playlists
ng g p shared/pipes/yesno --export 


## Card component
ng g c shared/components/card --export 


## Tabs

ng g m shared/modules/tabs -m shared
ng g c shared/modules/tabs/tabs --export
ng g c shared/modules/tabs/tab --export

## Music Search

ng g m music-search --routing -m app

ng g c music-search/containers/album-search

ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card

ng g s core/services/music-api --flat false

```html
<div class="row">
    <div class="col">
        <app-search-form></app-search-form>
    </div>
</div>

<div class="row">
    <div class="col">
        <app-search-results></app-search-results>
    </div>
</div>
```
```html
<div class="card">
    <img src="https://www.placecage.com/c/300/300" class="card-img-top">

    <div class="card-body">
        <h5 class="card-title">Card title</h5>                
    </div>
</div>
```
```html
<div class="row row-cols-1 row-cols-sm-4 no-gutters">
    <div class="col mb-4">
        <app-album-card></app-album-card>
    </div>
    <div class="col mb-4">
        <app-album-card></app-album-card>
    </div>
    <div class="col mb-4">
        <app-album-card></app-album-card>
    </div>
    <div class="col mb-4">
        <app-album-card></app-album-card>
    </div>
</div>
```
```html
<div class="input-group mb-3">
    <input type="text" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
    <button class="btn btn-outline-secondary" type="button" id="button-addon2">Search</button>
</div>
```
```ts
const routes: Routes = [
  {
    path: 'search',
    component: AlbumSearchComponent
  }
];
```
```ts
const routes: Routes = [
  {
    path: '',
    redirectTo: 'search',
    pathMatch: 'full'
  }
];
```

## Dependency injection / providers
ng g m mocks -m app

## Auth
npm i angular-oauth2-oidc

https://www.npmjs.com/package/angular-oauth2-oidc
https://developer.spotify.com/documentation/general/guides/authorization-guide/#implicit-grant-flow

ng g s core/services/auth

## HTTP Interceptors
ng g interceptor core/auth/auth-interceptor

## Directives
ng g m shared/dropdown -m shared
ng g d shared/dropdown/dropdown --export 
ng g d shared/dropdown/dropdown-menu --export 
ng g d shared/dropdown/dropdown-toggle --export 


## Forms
```ts
form
// FormGroup {_hasOwnPendingAsyncValidator: false, _parent: null, pristine: true, touched: false, _onCollectionChange: ƒ, …}
form.get('query').value 
// "batman"
form.get('query').setValue('placki')
// undefined
form.value 
// {query: "placki", type: "album"}
form.setValue({query: "test", type: "artist"})
// undefined
form.patchValue({query:"asd"})
// undefined
form.valueChanges.subscribe(console.log)
// Subscriber {closed: false, _parentOrParents: null, _subscriptions: Array(1), syncErrorValue: null, syncErrorThrown: false, …}
// core.js:25933 {query: "asd1", type: "artist"}
// core.js:25933 {query:asd1", type: "album"}
```


## RxJS
https://rxjs-dev.firebaseapp.com/api/operators/multicast
https://rxmarbles.com/#mergeMap
https://rxviz.com/
https://rxjs-dev.firebaseapp.com/operator-decision-tree

node_modules\rxjs\internal\operators\map.js


## Recent search E
ng g c shared/containers/recent-search --export

https://en.wikipedia.org/wiki/Command%E2%80%93query_separation

subscribe results event 
send command event


## Album details
ng g c music-search/containers/album-details 


## i18n - Internationlization Localization
https://angular.io/guide/i18n

https://angular.io/guide/i18n#setting-up-locale


https://angular.io/api/common/registerLocaleData

import localeData from '@angular/common/locales/pl'
registerLocaleData(localeData)

https://github.com/angular/angular/blob/master/packages/common/locales/pl.ts
    { provide: LOCALE_ID, useValue: 'pl' },

https://angular.io/guide/i18n#i18n-pipes
https://angular.io/guide/i18n#add-the-localize-package

## Testing
ng test 

https://github.com/ngneat/spectator 

ng g s core/services/playlists-api
ng g s core/services/users-api


## BUild --prod
ng build --prod
✔ Browser application bundle generation complete.
✔ Copying assets complete.
✔ Index html generation complete.

Initial Chunk Files               | Names         |      Size
main.ce82946a3e468cd2cbf5.js      | main          | 410.82 kB
styles.4ece004c9ba69fab46ed.css   | styles        | 142.10 kB
polyfills.c123de647731fb4a5496.js | polyfills     |  36.67 kB
runtime.86dfb9c288f538c2e082.js   | runtime       |   2.28 kB

                                  | Initial Total | 591.87 kB

Lazy Chunk Files                  | Names         |      Size
4.2cf2579a00b013b18ffb.js         | -             |  13.39 kB

Build at: 2021-04-16T12:08:18.084Z - Hash: dd470bd5596ed48e368b - Time: 34158ms

Warning: initial exceeded maximum budget. Budget 500.00 kB was not met by 91.87 kB with a total of 591.87 kB.

## SOurce map exploerer
ng build --prod --source-map
npm i -g source-map-explorer

PC@DESKTOP-6HIPUQN MINGW64 /c/Projects/szkolenia/angular-zus (master)
$ cd dist/angular-zus/

PC@DESKTOP-6HIPUQN MINGW64 /c/Projects/szkolenia/angular-zus/dist/angular-zus (master)
$ source-map-explorer main.ce82946a3e468cd2cbf5.js
Unable to map 18502 / 420677 bytes (4.40%)

## Differential loading
.browserrc
```
# not IE 11 # Angular supports IE 11 only as an opt-in. To opt-in, remove the 'not' prefix on this line.
IE 11 # Angular supports IE 11 only as an opt-in. To opt-in, remove the 'not' prefix on this line.
```

## Deployment
https://angular.io/guide/deployment#routed-apps-must-fallback-to-indexhtml

### IIS: add a rewrite rule to web.config, similar to the one shown here:
content_copy
```xml
<system.webServer>
  <rewrite>
    <rules>
      <rule name="Angular Routes" stopProcessing="true">
        <match url=".*" />
        <conditions logicalGrouping="MatchAll">
          <add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
          <add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />
        </conditions>
        <action type="Rewrite" url="/index.html" />
      </rule>
    </rules>
  </rewrite>
</system.webServer>
```